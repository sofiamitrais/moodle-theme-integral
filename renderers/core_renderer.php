<?php

class theme_integral_core_renderer extends core_renderer 
{
    private function get_user_menu_action_menu($returnstr,$opts, $withlinks)
    {
        $li_attr = [ 'class' => 'profile-info dropdown' ];
        if (!$withlinks) 
        {
            return html_writer::tag('li', $returnstr, $li_attr);
        }

        $navitemcount = count($opts->navitems);
        $idx = 0;
        $li = [];
        $divider =  html_writer::tag('div', '', ['class' => 'dropdown-divider']);

        foreach ($opts->navitems as $key => $value) 
        {
            switch ($value->itemtype) 
            {
                case 'divider':
                    $li[] = $divider;
                    break;

                case 'invalid':
                    break;

                case 'link':
                    // Process this as a link item.
                    $pix = '';
                    if (isset($value->pix) && !empty($value->pix)) 
                    {
                    //     $pix = new pix_icon($value->pix, $value->title, null, array('class' => 'iconsmall'));
                    } 
                    else if (isset($value->imgsrc) && !empty($value->imgsrc)) 
                    {
                        $value->title = html_writer::img(
                            $value->imgsrc,
                            $value->title,
                            array('class' => 'iconsmall')
                        ) . $value->title;
                    }

                    $attr = [];
                    if (!empty($value->titleidentifier)) 
                    {
                        $attr['data-title'] = $value->titleidentifier;
                    }
                    $al = html_writer::link($value->url, $pix . $value->title, $attr);

                    $li[] = html_writer::tag('li', $al);
                    break;
            }

            // Add dividers after the first item and before the last item.
            $idx++;
            if ($idx == 1 || $idx == $navitemcount - 1) 
            {
                $li[] = $divider;
            }
        }

        $ul = html_writer::tag('ul', join('', $li), [ 
            'class' => 'dropdown-menu', 
            // 'aria-labelledby' => 'dropdownMenuLink' 
        ]);

        return html_writer::tag('li', $returnstr . $ul, $li_attr);
    }

    private function get_user_menu_user_text($opts)
    {
        $avatarcontents = html_writer::span($opts->metadata['useravatar'], 'avatar current');
        $usertextcontents = $opts->metadata['userfullname'];

        // Other user.
        if (!empty($opts->metadata['asotheruser'])) {
            $avatarcontents .= html_writer::span(
                $opts->metadata['realuseravatar'],
                'avatar realuser'
            );
            $usertextcontents = $opts->metadata['realuserfullname'];
            $usertextcontents .= html_writer::tag(
                'span',
                get_string(
                    'loggedinas',
                    'moodle',
                    html_writer::span(
                        $opts->metadata['userfullname'],
                        'value'
                    )
                ),
                array('class' => 'meta viewingas')
            );
        }

        // Role.
        if (!empty($opts->metadata['asotherrole'])) {
            $role = core_text::strtolower(preg_replace('#[ ]+#', '-', trim($opts->metadata['rolename'])));
            $usertextcontents .= html_writer::span(
                $opts->metadata['rolename'],
                'meta role role-' . $role
            );
        }

        // User login failures.
        if (!empty($opts->metadata['userloginfail'])) {
            $usertextcontents .= html_writer::span(
                $opts->metadata['userloginfail'],
                'meta loginfailures'
            );
        }

        // MNet.
        if (!empty($opts->metadata['asmnetuser'])) {
            $mnet = strtolower(preg_replace('#[ ]+#', '-', trim($opts->metadata['mnetidprovidername'])));
            $usertextcontents .= html_writer::span(
                $opts->metadata['mnetidprovidername'],
                'meta mnet mnet-' . $mnet
            );
        }

        return html_writer::link('#', $avatarcontents . $usertextcontents, [
            'data-toggle' => 'dropdown',
            'class' => 'dropdown-toggle',
            'aria-expanded' => 'true',
            'data-toggle' => 'dropdown',
            'aria-haspopup' => 'true',
            'role' => 'button',
        ]);
    }

    private function get_user_menu_login_menu($text, $is_show_login_link = true)
    {
        $loginurl = get_login_url();
        $isloginpage = $this->is_login_page();

        if (!$isloginpage && $is_show_login_link) 
        {
            $text .= " (<a href=\"$loginurl\">".get_string('login').'</a>)';
        }

        return html_writer::div(
            html_writer::span(
                $text,
                'login'
            )
        );
    }

    /**
     * Construct a user menu, returning HTML that can be echoed out by a
     * layout file.
     *
     * @param stdClass $user A user object, usually $USER.
     * @param bool $withlinks true if a dropdown should be built.
     * @return string HTML fragment.
     */
    public function user_menu($user = null, $withlinks = null) 
    {
        global $USER, $CFG;
        require_once($CFG->dirroot . '/user/lib.php');

        if (is_null($user)) 
        {
            $user = $USER;
        }

        if (is_null($withlinks)) 
        {
            $withlinks = empty($this->page->layout_options['nologinlinks']);
        }

        // If during initial install, return the empty return string.
        if (during_initial_install()) 
        {
            return '';
        }

        // If not logged in, show the typical not-logged-in string.
        if (!isloggedin()) 
        {
            return $this->get_user_menu_login_menu(get_string('loggedinnot', 'moodle'));
        }
        // If logged in as a guest user, show a string to that effect.
        if (isguestuser()) 
        {
            return $this->get_user_menu_login_menu(get_string('loggedinasguest'), $withlinks);
        }

        // Get some navigation opts.
        $opts = user_get_user_navigation_info($user, $this->page);
        // user info

        $usertext = $this->get_user_menu_user_text($opts);
        $menu = $this->get_user_menu_action_menu($usertext, $opts, $withlinks);
        $options = [ 
            'class' => 'user-info pull-left'  . (!$withlinks? ' withoutlinks' : '') 
        ];

        return html_writer::tag('ul', $menu, $options);
    }


    /**
     * Return the navbar content so that it can be echoed out by the layout
     *
     * @return string XHTML navbar
     */
	public function navbar() 
	{
	    $items = $this->page->navbar->get_items();
        $itemcount = count($items);
        if ($itemcount === 0) {
            return '';
        }

        $htmlblocks = array();
        $breadcrumboption = [ 'class' => 'breadcrumb-item' ];
        for ($i=0; $i < $itemcount; $i++) 
		{
            $item = $items[$i];
            $item->hideicon = true;
            $content = html_writer::tag('li', $this->render($item), $breadcrumboption);
            $htmlblocks[] = $content;
        }

        $navbarcontent = html_writer::tag('ol', join('', $htmlblocks), [ 'class' => 'breadcrumb breadcrumb-2' ]);
        
		// XHTML
        return $navbarcontent;
    }

}