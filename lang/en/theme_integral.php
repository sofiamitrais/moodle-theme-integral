<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin strings are defined here.
 *
 * @package     theme_integral
 * @category    string
 * @copyright   2017 Sofia
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Integral';
$string['configtitle'] = 'Integral';
$string['themegeneralsettings'] = 'General Settings';
$string['logo'] = 'Logo';
$string['logodesc'] = 'Logo of the page';
$string['topmenufontsize'] = 'Font Size';
$string['topmenufontsizedesc'] = 'Font Size Of Menu';
$string['choosereadme'] = 'Integral Theme by Sofia';


$string['footertext'] = 'Footer Text';
$string['footertextdesc'] = 'Text at Footer';
