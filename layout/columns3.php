<?php

$html = theme_integral_get_html_for_settings($OUTPUT, $PAGE);

$PAGE->requires->js('/theme/integral/javascript/general.js');
echo $OUTPUT->doctype() ?>
<html>
<head>
	<title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body <?php echo $OUTPUT->body_attributes('two-column'); ?>>
	<?php echo $OUTPUT->standard_top_of_body_html() ?>

	<div class="page-container">
		
		<?php  require_once(dirname(__FILE__) . '/includes/sidebar.php');  ?>

		<div class="main-container">
			<?php  require_once(dirname(__FILE__) . '/includes/header.php');  ?>

			<div class="main-content">
				<div class="pull-right heading-button">
				<?php echo $OUTPUT->page_heading_button(); ?>
				</div>

				<?php echo $OUTPUT->page_heading(); ?>
				<?php echo $OUTPUT->navbar(); ?>

				<div class="row">
					<div class="col-sm-8">
					<?php
						echo $OUTPUT->course_content_header();
						echo $OUTPUT->main_content();
						echo $OUTPUT->course_content_footer();
					?>
					</div>
					<div class="col-sm-4" id="post-block-region-content">
						<?php echo $OUTPUT->blocks('side-post'); ?>
					</div>
			</div><!-- /main-content -->

			<?php  require_once(dirname(__FILE__) . '/includes/footer.php');  ?>
			
		</div><!-- /main-container -->
	
	</div><!-- /page-container -->
    <?php echo $OUTPUT->standard_end_of_body_html() ?>
</body>
</html>