<?php
$PAGE->requires->js('/theme/integral/javascript/general.js');
$logo = theme_integral_get_logo_url();
$blockshtml = $OUTPUT->blocks('side-pre');
$templatecontext = [
    'output' => $OUTPUT,
    'sidepreblocks' => $blockshtml,
    'logo' => $logo,
    'homeurl' => $CFG->wwwroot,
    'node' => $PAGE->settingsnav->find('courseadmin', navigation_node::TYPE_COURSE),
];
echo $OUTPUT->render_from_template('theme_integral/course', $templatecontext);
