<?php
$PAGE->requires->js('/theme/integral/javascript/general.js');
$logo = theme_integral_get_logo_url();
$blockshtml = $OUTPUT->blocks('side-pre');
$templatecontext = [
    'output' => $OUTPUT,
    'sidepreblocks' => $blockshtml,
    'logo' => $logo,
    'homeurl' => $CFG->wwwroot,
];
echo $OUTPUT->render_from_template('theme_integral/columns2', $templatecontext);
