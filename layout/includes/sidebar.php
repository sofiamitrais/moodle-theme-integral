<div class="page-sidebar">
	<header class="site-header clearfix">
		<div class="site-logo">
			<a href="<?php echo $CFG->wwwroot;?>">
            	<img src="<?php echo theme_integral_get_logo_url(); ?>" alt="Integral">
            </a>
		</div>
	</header>
	<?php echo $OUTPUT->blocks('side-pre'); ?>
</div><!-- /page-sidebar -->