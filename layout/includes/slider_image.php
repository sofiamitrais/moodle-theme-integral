<!-- Jssor Slider Begin -->

<div id="slider1_container">

    <!-- Slides Container -->
    <div class="slides" u="slides">
        <div>
            <img data-u="image" src="<?php echo $OUTPUT->image_url('img/gallery/980x380/001', 'theme'); ?>" />
        </div>
        <div>
            <img data-u="image" src="<?php echo $OUTPUT->image_url('img/gallery/980x380/002', 'theme'); ?>" />
        </div>
        <div>
            <img data-u="image" src="<?php echo $OUTPUT->image_url('img/gallery/980x380/003', 'theme'); ?>" />
        </div>
        <div>
            <img data-u="image" src="<?php echo $OUTPUT->image_url('img/gallery/980x380/004', 'theme'); ?>" />
        </div>
    </div>
    
    <!--#region Bullet Navigator Skin Begin -->
    <!-- Help: https://www.jssor.com/development/slider-with-bullet-navigator.html -->
    <div data-u="navigator" class="jssorb031" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
        <div data-u="prototype" class="i" style="width:16px;height:16px;">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="b" cx="8000" cy="8000" r="5800"></circle>
            </svg>
        </div>
    </div>
    <!--#endregion Bullet Navigator Skin End -->

    <!--#region Arrow Navigator Skin Begin -->
    <div data-u="arrowleft" class="jssora051" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
        </svg>
    </div>
    <div data-u="arrowright" class="jssora051" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
        </svg>
    </div>
    <!--#endregion Arrow Navigator Skin End -->

</div>
<!-- Jssor Slider End -->
