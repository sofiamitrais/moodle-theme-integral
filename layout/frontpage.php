<?php

$html = theme_integral_get_html_for_settings($OUTPUT, $PAGE);

$PAGE->requires->js('/theme/integral/javascript/general.js');
$PAGE->requires->js('/theme/integral/javascript/jssor.slider.min.js');

echo $OUTPUT->doctype() ?>
<html>
<head>
	<title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body <?php echo $OUTPUT->body_attributes('two-column'); ?>>
	<?php echo $OUTPUT->standard_top_of_body_html() ?>
	<div class="page-container">

		<div class="main-container">
			<div class="main-header row">
				<?php echo $OUTPUT->user_menu(); ?>
			</div><!-- /main-header -->

			<?php  require_once(dirname(__FILE__) . '/includes/slider_image.php');  ?>

			<div class="main-content">
				<?php
					echo $OUTPUT->course_content_header();
					echo $OUTPUT->main_content();
					echo $OUTPUT->course_content_footer();
				?>
			</div><!-- /main-content -->

			<?php  require_once(dirname(__FILE__) . '/includes/footer.php');  ?>
			
		</div><!-- /main-container -->
	
	</div><!-- /page-container -->
    <?php echo $OUTPUT->standard_end_of_body_html() ?>
    <?php  require_once(dirname(__FILE__) . '/includes/slider_javascript.php');  ?>
</body>
</html>