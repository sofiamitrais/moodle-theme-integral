require(['jquery'], function($) {
    $(function(){
    	$('.dropdown-toggle').click(function(e){
    		e.preventDefault();
    		$(this).siblings('.dropdown-menu').toggle();
    		return false;
    	});

        if(localStorage.getItem('sidebar') == 'hide')
        {
            $('.page-sidebar').css({ left : '-280px' });
            $('.main-container').css({ 'padding-left' : 0 });
        }

        $('#menu-bar-link').click(function(e){

            e.preventDefault();

            if($('.main-container').css('padding-left') === '280px')
            {
                localStorage.setItem('sidebar', 'hide');
                $('.page-sidebar').animate({ left : '-280px' });
                $('.main-container').animate({ 'padding-left' : 0 });
            }
            else
            { 
                localStorage.setItem('sidebar', 'show');
                $('.page-sidebar').animate({ left : 0 });
                $('.main-container').animate({ 'padding-left' : '280px' });
            }

            return false;
        });

    	$('.nav-item .nav-link').click(function(e){
    		e.preventDefault();

    		let nav = $(this).closest('.nav-tabs');
    		nav.find('.nav-item > .nav-link').removeClass('active');
    		$(this).addClass('active');

    		let tp = nav.siblings('.tab-content:first').children('.tab-pane');
    		tp.removeClass('active in');
    		let hr = $(this).attr('href');
    		$(hr).addClass('active in');
    	});

    	$('body').on('click', 'a[data-toggle="tab"]', function(e) {
    		e.preventDefault();
    		$(this).addClass('active');

    		let tp = $(this).parent().parent().siblings('.tab-content').children('.tab-pane');
    		tp.removeClass('active in');
    		let hr = $(this).attr('href');
    		$(hr).addClass('active in');
    	});
	});    
});