<?php

//your theme name
$THEME->name = 'integral';
//your theme document type
$THEME->doctype = '1';
//your theme parents
$THEME->parents = [ 'boost' ];
//css files you want to add, this will add file style\general.css
$THEME->sheets = [ 'general' ];
//function that call after process css
$THEME->csspostprocess = 'theme_integral_process_css';

$THEME->rendererfactory = 'theme_overridden_renderer_factory';

$THEME->scss = function($theme){
	return theme_integral_get_main_scss_content($theme);
};

$THEME->layouts = [
	'mydashboard' => [
		'theme' => 'integral',
        'file' => 'columns3.php',
        'regions' => [ 'side-pre', 'side-post'],
        'defaultregion' => 'side-pre',
        'options' => [ 'langmenu' => true ],
    ],
    'frontpage' => [
        'file' => 'frontpage.php',
        'regions' => ['side-pre'],
        'defaultregion' => 'side-pre',
        'options' => ['nonavbar' => true],
    ],
    'course' => [
        'file' => 'course.php',
        'regions' => ['side-pre'],
        'defaultregion' => 'side-pre',
        'options' => ['nonavbar' => true],
    ],
];

$THEME->iconsystem = \core\output\icon_system::FONTAWESOME;