<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin administration pages are defined here.
 *
 * @package     theme_integral
 * @category    admin
 * @copyright   2017 Sofia
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if (is_siteadmin()) {
    $settings = new theme_boost_admin_settingspage_tabs('themesettingintegral', get_string('configtitle', 'theme_integral'));
    $ADMIN->add('themes', new admin_category('theme_integral', 'integral'));

    /* Header Settings */
    $temp = new admin_settingpage('theme_integral_header', get_string('themegeneralsettings', 'theme_integral'));

    // Logo file setting.
    $name = 'theme_integral/logo';
    $title = get_string('logo', 'theme_integral');
    $description = get_string('logodesc', 'theme_integral');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo', 0, [ 
        'accepted_types' => ['.jpeg', '.png' ]
    ]);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

   // Top Menu Font Size.
    $name = 'theme_integral/topmenufontsize';
    $title = get_string('topmenufontsize', 'theme_integral');
    $description = get_string('topmenufontsizedesc', 'theme_integral');
    $radchoices = $standardfontsize;
    $setting = new admin_setting_configselect($name, $title, $description, '14px', $radchoices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Foot note setting.
    $name = 'theme_integral/footertext';
    $title = get_string('footertext', 'theme_integral');
    $description = get_string('footertextdesc', 'theme_integral');
    $setting = new admin_setting_confightmleditor($name, $title, $description, '');
    $temp->add($setting);

    $settings->add($temp);
}
