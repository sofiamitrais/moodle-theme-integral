<?php

function theme_integral_get_html_for_settings(renderer_base $output, moodle_page $page) {
    global $CFG;
    $return = new stdClass;

    return $return;
}

function theme_integral_process_css($css, $theme)
{
    $tag = '[[setting:topmenufontsize]]';
    $replacement = $theme->settings->topmenufontsize;
    if (is_null($replacement)) 
    {
        $replacement = '20px';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_integral_get_logo_url() 
{
    global $OUTPUT, $PAGE;

    $logo = $PAGE->theme->setting_file_url('logo', 'logo');
    $logo = empty($logo) ? $OUTPUT->image_url('home/logo', 'theme') : $logo;
    
    return $logo;
}

function theme_integral_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = array()) {
    static $theme;

    if (empty($theme)) {
        $theme = theme_config::load('integral');
    }
    if ($context->contextlevel == CONTEXT_SYSTEM) 
    {
        if ($filearea === 'logo') {
            return $theme->setting_file_serve('logo', $args, $forcedownload, $options);
        }else {
            send_file_not_found();
        }
    } else {
        send_file_not_found();
    }
}

function theme_integral_get_main_scss_content($theme) {
    global $CFG;

    $scss = '';
    $filename = !empty($theme->settings->preset) ? $theme->settings->preset : null;
    $fs = get_file_storage();

    $context = context_system::instance();
    if ($filename == 'default.scss') {
        // We still load the default preset files directly from the boost theme. No sense in duplicating them.
        $scss .= file_get_contents($CFG->dirroot . '/theme/boost/scss/preset/default.scss');
    } else if ($filename == 'plain.scss') {
        // We still load the default preset files directly from the boost theme. No sense in duplicating them.
        $scss .= file_get_contents($CFG->dirroot . '/theme/boost/scss/preset/plain.scss');

    } else if ($filename && ($presetfile = $fs->get_file($context->id, 'theme_integral', 'preset', 0, '/', $filename))) {
        // This preset file was fetched from the file area for theme_photo and not theme_boost (see the line above).
        $scss .= $presetfile->get_content();
    } else {
        // Safety fallback - maybe new installs etc.
        $scss .= file_get_contents($CFG->dirroot . '/theme/boost/scss/preset/default.scss');
    }

    // Pre CSS - this is loaded AFTER any prescss from the setting but before the main scss.
    $pre = file_get_contents($CFG->dirroot . '/theme/integral/scss/pre.scss');
    // Post CSS - this is loaded AFTER the main scss but before the extra scss from the setting.
    $post = file_get_contents($CFG->dirroot . '/theme/integral/scss/post.scss');

    // Combine them together.
    return $pre . "\n" . $scss . "\n" . $post;
}